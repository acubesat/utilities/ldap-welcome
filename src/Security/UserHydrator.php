<?php


namespace App\Security;


use App\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use FR3D\LdapBundle\Driver\LdapDriverInterface;
use FR3D\LdapBundle\Driver\ZendLdapDriver;
use FR3D\LdapBundle\Hydrator\AbstractHydrator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Zend\Ldap\Ldap;

class UserHydrator extends AbstractHydrator
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var Ldap
     */
    private $driver;

    private $attributeMap;

    public function __construct(UserManagerInterface $userManager, Ldap $driver, array $attributeMap)
    {
        parent::__construct($attributeMap);

        $this->userManager = $userManager;
        $this->driver = $driver;
        $this->attributeMap = $attributeMap;
    }

    protected function createUser()
    {
        $user = $this->userManager->createUser();
        $user->setPassword('');



        if ($user instanceof AdvancedUserInterface) {
            $user->setEnabled(true);
        }

        return $user;
    }

    protected function hydrateUserWithAttributesMap(UserInterface $user, array $ldapUserAttributes, array
    $attributeMap):
    void
    {
        parent::hydrateUserWithAttributesMap($user, $ldapUserAttributes, $attributeMap);

        $managers = $this->driver->getEntry($_ENV['LDAP_MANAGERS_DN']);

        foreach (($managers['roleoccupant'] ?? [])  as $manager) {
            if ($manager == $ldapUserAttributes['dn']) {
                $user->setRoles(['ROLE_SUPER_ADMIN']);
            }
        }
    }


}

