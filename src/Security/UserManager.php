<?php


namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;

/**
 * Extend FOSUserBundle's UserManager so that it does not store any users in the database
 */
class UserManager extends \FOS\UserBundle\Doctrine\UserManager
{
    public function __construct(PasswordUpdaterInterface $passwordUpdater, CanonicalFieldsUpdater $canonicalFieldsUpdater, EntityManagerInterface $om)
    {
        parent::__construct($passwordUpdater, $canonicalFieldsUpdater, $om, \App\Entity\User::class);
    }

    public function deleteUser(UserInterface $user)
    {
    }

    public function updateUser(UserInterface $user, $andFlush = true)
    {
    }

    public function createUser()
    {
        $class = $this->getClass();
        return new $class();
    }
}
