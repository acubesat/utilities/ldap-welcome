<?php

namespace App\Entity;

use App\LDAP\UserCreationRequest;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Token
{
    const TYPE_USER_CREATE = 0;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $createdBy;

    /**
     * @ORM\Column(type="integer")
     */
    private $type = 0;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var Carbon
     */
    private $expiry;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    /**
     * @ORM\Column(type="object", nullable=true)
     * @var UserCreationRequest
     */
    private $data = null;

    public function __construct()
    {
        $this->code = bin2hex(random_bytes(16));
        $this->data = new UserCreationRequest();
        $this->renew();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Token
     */
    public function setCreatedBy($createdBy): self
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getExpiry(): ?Carbon
    {
        if ($this->expiry === null) {
            return null;
        }

        return Carbon::instance($this->expiry);
    }

    public function setExpiry(?\DateTimeInterface $expiry): self
    {
        $this->expiry = $expiry;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function isValid()
    {
        return $this->active && (
                $this->getExpiry() === null ||
                $this->getExpiry()->isFuture()
            );
    }

    /**
     * @return UserCreationRequest
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param UserCreationRequest $data
     * @return Token
     */
    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Renew expiring date, starting from today
     * @return Token
     */
    public function renew(): self
    {
        $this->expiry = Carbon::now();
        $this->expiry->addMonths(2);

        return $this;
    }
}
