<?php


namespace App\Operations;


use App\LDAP\UserCreationRequest;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Log\LoggerInterface;

class Mattermost
{
    /**
     * Guzzle HTTP client
     */
    private $client;

    private $api;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
      * @var Snippets
     */
    private $snippets;

    public function __construct(LoggerInterface $logger, Snippets $snippets)
    {
        $handlerStack = HandlerStack::create();
        $handlerStack->push(Middleware::log($logger, new MessageFormatter(MessageFormatter::SHORT)));

        $this->client = new Client([
            'handler' => $handlerStack,
            'headers' => [
                'Authorization' => 'Bearer ' . $_ENV['MATTERMOST_TOKEN'],
            ],
        ]);

        $this->api = rtrim($_ENV['URL_MATTERMOST'], '/'). '/api/v4';

        $this->logger = $logger;
        $this->snippets = $snippets;
    }

    public function createUser(UserCreationRequest $ucr)
    {
        $this->logger->info('Mattermost account temporarily disabled');
/*        $request = $this->client->get($this->api . "/teams/name/" . $_ENV['MATTERMOST_TEAM']);
        $team = json_decode($request->getBody()->getContents(), true);

        $request = $this->client->post($this->api . "/users", [
            'query' => [ 'iid' => $team['invite_id']],
            'body' => json_encode([
                'email' => $ucr->email,
                'username' => $ucr->username,
                'first_name' => $ucr->firstName,
                'last_name' => $ucr->lastName,
                'password' => $ucr->password,
                'auth_service' => 'gitlab',
                'notify_props' => [
                    'push' => 'all'
                ]
            ])
        ]);
        $user = json_decode($request->getBody()->getContents(), true);

        $this->logger->info('Attempted user creation', $user); */
    }

    public function sendAlert(UserCreationRequest $ucr)
    {
        $request = $this->client->get($this->api . "/teams/name/" . $_ENV['MATTERMOST_TEAM'] . "/channels/name/" .
            $_ENV['MATTERMOST_CHANNEL']);
        $channel = json_decode($request->getBody()->getContents(), true);

        $snippet = $this->snippets->getSnippet($_ENV['GITLAB_JOINSNIPPET']);
        $snippet = str_replace('{DRIVE_EMAIL}', $ucr->googleDriveEmail, $snippet);
        $snippet = str_replace('{EMAIL}', $ucr->email, $snippet);

        $request = $this->client->post($this->api . "/posts", [
            'body' => json_encode([
                'channel_id' => $channel['id'],
                'message' =>
                    "**#New_Member**
We have a new member (**{$ucr->firstName} `{$ucr->username}` {$ucr->lastName}**). Subsystem **{$ucr->subsystem}**.

Tasks done:
- Mattermost account NOT created
    - Welcome message will be sent automatically
- OCDT account created
- Openproject account created

Tasks that need to be done:
$snippet
"

            ])
        ]);
        $post = json_decode($request->getBody()->getContents(), true);

        $this->logger->info('Sent alert', $post);
    }


}
