<?php

namespace App\Operations;

/**
 * A class used to fetch snippets from GitLab or any other services,
 * to be used as text throughout the application
 */
class Snippets {
    /**
     * Get the content of a snippet by its ID
     *
     * @param string $id The ID of the snippet. Not escaped, passed raw.
     * @return string The content of the snippet
     */
    function getSnippet($id) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['PRIVATE-TOKEN: ' .  $_ENV['GITLAB_TOKEN']]);
        curl_setopt($curl, CURLOPT_URL, "https://gitlab.com/api/v4/snippets/$id/raw"); 

        return curl_exec($curl);
    }
}
