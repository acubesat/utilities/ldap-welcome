<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Token;
use App\LDAP\Database;
use Sonata\AdminBundle\Controller\CRUDController;

final class TokenAdminController extends CRUDController
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @required
     * @param Database $database
     */
    public function setDatabase(Database $database)
    {
        $this->database = $database;
    }

    public function listUsersAction()
    {
        return $this->renderWithExtraParams('admin/list_users.html.twig', [
            'users' => $this->database->getAllUsers()
        ]);
    }

    public function listSubsystemsAction()
    {
        $subsystems = []; // The format that will be passed to the view
        $subsystemless = [];

        foreach ($this->database->getAllSubsystems() as $subsystem) {
            $subsystems[$subsystem->getDistinguishedName()] = [
                'subsystem' => $subsystem,
                'users' => []
            ];
        }
        // Add users to each subsystem
        foreach ($this->database->getAllUsers() as $user) {
            foreach (($user->getAttribute('xgrasatSubsystemName') ?? []) as $subsystemDn) {
                $subsystems[$subsystemDn]['users'][] = $user;
            }

            if (empty($user->getAttribute('xgrasatSubsystemName'))) {
                // Do something if the user belongs in no subsystems
                $subsystemless[] = $user;
            }
        }
        $subsystems['Without subsystem'] = [
            'users' => $subsystemless
        ];

        return $this->renderWithExtraParams('admin/list_subsystems.html.twig', [
            'subsystems' => $subsystems
        ]);
    }

    protected function redirectTo($object)
    {
        return $this->createdAction($object);
    }


    public function createdAction(Token $token)
    {
        return $this->renderWithExtraParams('admin/token_created.html.twig', [
            'token' => $token
        ]);
    }
}
