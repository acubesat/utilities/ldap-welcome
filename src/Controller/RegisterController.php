<?php

namespace App\Controller;

use App\Entity\Token;
use App\LDAP\Database;
use App\LDAP\UserCreationRequest;
use App\Operations\Mattermost;
use FR3D\LdapBundle\Validator\Unique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use function Symfony\Component\DependencyInjection\Loader\Configurator\iterator;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function index(Request $request, Database $database, Mattermost $mattermost)
    {
        // Basic options
        $bo = ['required' => false, 'constraints' => new NotBlank()];

        if (false) { // Debugging
            $ucr = new UserCreationRequest();
        } else {
            $tokenCode = $request->query->get('token');
            $token = $this->getDoctrine()->getRepository('App:Token')->findOneByCode($tokenCode);
            // No token found
            if (!$token) {
                $this->addFlash('danger', 'Invalid request. Please ask for a registration link.');
                return $this->redirectToRoute('landing');
            }
            /** @var Token $token */
            // Token is expired or disabled
            if (!$token->isValid()) {
                $this->addFlash('danger', 'Registration expired. Please ask for a new link.');
                return $this->redirectToRoute('landing');
            }

            $ucr = $token->getData();
        }

        $builder = $this->createFormBuilder($ucr)
            ->add('firstName', TextType::class, $bo)
            ->add('lastName', TextType::class, $bo)
            ->add('username', TextType::class,  [
                'required' => false,
                'help' => 'Create a username that you will use to sign in to all the services',
                'attr' => ['autocomplete' => 'off'],
                'constraints' => [
                    new NotBlank(),
                    new Callback(function($object, ExecutionContextInterface $context, $payload) use ($database) {
                        if ($database->getUserByUsername($object) !== null) {
                            $context->buildViolation('User with this username already exists.')->addViolation();
                        }
                    })
                ]
            ])
            ->add('email', EmailType::class, $bo + [
                'help' => 'The email that will be used for your accounts and notifications. Note that this address will be visible to all team members.',
                'attr' => ['autocomplete' => 'off'],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => false,
                'first_options'  => [
                    'label' => 'Password',
                    'required' => false,
                    'help' => 'Set a strong password that you will use to connect to our services',
                    'attr' => ['autocomplete' => 'new-password'],
                ],
                'second_options' => ['label' => 'Confirm Password', 'required' => false, 'help' => 'Type the same password as above'],
                'constraints' => [
                    new NotBlank(), new Length(['min' => 6])
                ]
            ])
            ->add('subsystem', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(iterator_to_array($database->getSubsystems())),
                'attr' => ['autocomplete' => 'off'],
            ])
            ->add('googleDriveEmail', EmailType::class, $bo + [
                'label' => 'Google Drive Email',
                'help' => 'An e-mail connected to a Google account that you will use to access the team\'s Google Drive repository'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Submit!'
            ]);

        foreach($builder->all() as $input) {
            if (($builder->getData()->{$input->getName()} ?? null) !== null) {
                $input->setDisabled(true);
            }
        }

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // We can create the user account
            $form->getData()->sanitize();
            $database->createUser($form->getData());
            $mattermost->createUser($form->getData());
            $this->createOCDTUser($form->getData());

            // Clear the token
            if (isset($token)) {
                $token->setActive(false);
                $this->getDoctrine()->getManager()->persist($token);
                $this->getDoctrine()->getManager()->flush();
            }

            $mattermost->sendAlert($ucr);

            $this->addFlash('success', "New account successfully created!");

            return $this->forward(LandingController::class . '::index', [
                'user' => $form->getData()->getCn()
            ]);
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView()
        ]);
    }


    private function createOCDTUser(UserCreationRequest $ucr) {
        $process = new Process([($_ENV['NODE_PATH'] ?? 'node'), __DIR__ . '/../../typescript_sdk/new_user.js',
            $ucr->username,
            $ucr->firstName,
            $ucr->lastName,
            $ucr->password
        ]);

        $process->run();
    }
}
