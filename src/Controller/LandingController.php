<?php

namespace App\Controller;

use App\LDAP\UserCreationRequest;
use App\Operations\Mattermost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    /**
     * @Route("/", name="landing")
     */
    public function index($user = null)
    {
        return $this->render('landing/index.html.twig', [
            'user' => $user,
            'urls' => [
                $_ENV['URL_PASSRESET'],
                $_ENV['URL_MATTERMOST'],
                $_ENV['URL_OPENPROJECT'],
                $_ENV['URL_OCDT'],
                $_ENV['URL_OCDT_WV'],
                $_ENV['URL_MINUTES'],
                $_ENV['URL_LDAP'],
            ]
        ]);
    }
}
