<?php


namespace App\LDAP;


use Adldap\Adldap;
use Adldap\Connections\ProviderInterface;
use Adldap\Models\Entry;
use Adldap\Query\Collection;
use Psr\Log\LoggerInterface;
use Zend\Ldap\Exception\LdapException;

class Database
{
    /**
     * @var ProviderInterface
     */
    private $provider;

    public function __construct(LoggerInterface $logger)
    {
        Adldap::setLogger($logger);

        // Construct new Adldap instance.
        $ad = new \Adldap\Adldap();

        // Create a configuration array.
        $config = [
            // An array of your LDAP hosts. You can use either
            // the host name or the IP address of your host.
            'hosts'    => [$_ENV['LDAP_HOST']],

            // The base distinguished name of your domain to perform searches upon.
            'base_dn'  => $_ENV['LDAP_BASEDN'],

            // The account to use for querying / modifying LDAP records. This
            // does not need to be an admin account. This can also
            // be a full distinguished name of the user account.
            'username' => $_ENV['LDAP_USERNAME'],
            'password' => $_ENV['LDAP_PASSWORD'],
        ];

        // Add a connection provider to Adldap.
        $ad->addProvider($config);

        // If a successful connection is made to your server, the provider will be returned.
        $this->provider = $ad->connect();
    }

    /**
     * Get a list of all the subsystems, in [ alias => full_name ] format
     * @return \Generator
     */
    public function getSubsystems()
    {
        $results = $this->provider->search()->in($_ENV['LDAP_SUBSYSTEMS_DN'])
            ->where('objectClass', '!', 'organizationalUnit')
            ->sortBy('cn', 'asc')
            ->get();

        foreach($results as $item) {
            /** @var Entry $item */
            yield ($item->cn[0]) => ($item->name[0] ?? $item->cn[0]);
        }
    }

    /**
     * Get a user by the username. Returns null if the user is not found
     * @param string $username
     * @return Entry|array
     */
    public function getUserByUsername(string $username)
    {
        $entry = $this->provider->search()->in($_ENV['LDAP_MEMBERS_DN'])
            ->where('uid', $username)
            ->first();

        if (!$entry || !$entry->exists) {
            return null;
        }
        
        return $entry->getAttributes();
    }

    /**
     * Get all the stored users
     * @return Entry[]
     */
    public function getAllUsers()
    {
        $users = $this->provider->search()->in($_ENV['LDAP_MEMBERS_DN'])
            ->where('objectClass', '!', 'organizationalUnit')
            ->sortBy('cn', 'asc')
            ->get();

        // Add the subsystem CNs to the user so they can just be shown later
        foreach ($users as &$user) {
            /** @var Entry $user */
            $subsystems = [];
            foreach (($user->getAttribute('xgrasatSubsystemName') ?? []) as $subsystem) {
                if (preg_match('/^cn=([^,]*)/', $subsystem, $matches)) {
                    $subsystems[] = $matches[1];
                }
            }
            $user->subsystems = $subsystems;
        }

        return $users;
    }

    /**
     * Get all the stored subsystems
     * @return Entry[]
     */
    public function getAllSubsystems()
    {
        return $this->provider->search()->in($_ENV['LDAP_SUBSYSTEMS_DN'])
            ->where('objectClass', '!', 'organizationalUnit')
            ->sortBy('cn', 'asc')
            ->get();
    }

    /**
     * Store a new user to the LDAP database
     * @param UserCreationRequest $user
     */
    public function createUser(UserCreationRequest $user)
    {
        $entry = $this->provider->make()->entry($user->convertToLdapAttributes());

        if( !@$entry->save() ) {
            throw new LdapException(null,
                $this->provider->getConnection()->getDetailedError()->getErrorMessage() . " " . $this->provider->getConnection()
                ->getDiagnosticMessage());
        }
    }
}
