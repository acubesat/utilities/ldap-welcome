<?php


namespace App\LDAP;

use phpDocumentor\Reflection\Types\Void_;
use Symfony\Component\Validator\Constraints as Assert;

class UserCreationRequest
{
    /**
     * @Assert\Regex(pattern="/^[\wa-zA-Z0-9\-\. ]*$/",message="Your name must contain English characters only")
     */
    public $firstName;
    /**
     * @Assert\Regex(pattern="/^[\wa-zA-Z0-9\-\. ]*$/",message="Your name must contain English characters only")
     */
    public $lastName;
    /**
     * @Assert\Regex(pattern="/^[\wa-zA-Z0-9\-\.]*$/",message="Your username must contain English characters only")
     */
    public $username;
    public $email;
    public $password;
    public $phone;
    public $subsystem;
    public $googleDriveEmail;

    /**
     * Get the common name
     * @return string
     */
    public function getCn()
    {
        return "{$this->firstName} {$this->lastName}";
    }

    /**
     * Sanitise some internal data before storing
     * @return void
     */
    public function sanitize()
    {
        $this->firstName = ucwords(strtolower($this->firstName));
        $this->lastName = ucwords(strtolower($this->lastName));
        $this->username = lcfirst($this->username);
    }

    /**
     * Convert this request to a list of LDAP attributes to be sent to the database
     * @return array
     */
    public function convertToLdapAttributes()
    {
        $attribs = [
            'objectClass' => ['xgrasatPerson'],
            'cn' => $this->getCn(),
            'dn' => 'cn=' . $this->getCn() . "," . $_ENV['LDAP_MEMBERS_DN'],
            'givenName' => $this->firstName,
            'sn' => $this->lastName,
            'email' => $this->email,
            'telephoneNumber' => $this->phone,
            'userPassword' => "{SHA512}" . base64_encode(hash('sha512', $this->password, true)),
            'xgrasatSubsystemName' => $this->subsystem ? ('cn=' . $this->subsystem . "," .
                $_ENV['LDAP_SUBSYSTEMS_DN']) :
                null,
            'uid' => $this->username,
            'xgrasatGdriveEmail' => $this->googleDriveEmail,
            'mailForwardingAddress' => $this->email,
        ];

        foreach ($attribs as $key => $value) {
            // Remove null value pairs
            if ($value === null) {
                unset($attribs[$key]);
            }
        }

        return $attribs;
    }

    public function __toString()
    {
        return $this->getCn();
    }

}
