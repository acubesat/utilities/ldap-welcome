**LDAP Welcome** is a web app that allows users to register for an LDAP environment,
creating their accounts and setting everything up.

It is tailored to AcubeSAT's needs, meaning it provides integrations with:
*  Mattermost (creates a Mattermost account, sends mattermost messages)
*  OCDT (creates an OCDT account)

![screenshot](./docs/screenshot.png)

### Installation instructions
1. Install OpenLDAP. If you get issues about support of hashes, configure LDAP with:
```
    ./configure --enable-modules --enable-crypt --enable-mdb --prefix="/usr" --libexecdir=/usr/sbin --sysconfdir=/etc
```
2. Create a `cn=Managers` entry under LDAP's baseDn that contains a `roleOccupant`
   field, which in turn contains the dns of users who should have administrative
   access to the application
2. Install [composer](https://getcomposer.org/)
3. Run `composer install`
4. Create a mattermost bot account, and keep the token.
5. Configure the application by copying `.env` to `.env.local` and modifying the
   values accordingly.
6. Initialise the database with `bin/console -e prod doctrine:schema:update`

To create a new user account, an administrator needs to log in with their
LDAP credentials, and create a new user. A URL with a single-use token is
then provided to the administrator.
